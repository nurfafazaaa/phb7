// backend.go

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type ph_data struct {
	ID        int       `json:"id"`
	AdminID   string    `json:"admin_id"`
	Value     float64   `json:"value"`
	Timestamp time.Time `json:"timestamp"`
}

type activity_log struct {
	ID        int       `json:"id"`
	AdminID   string    `json:"admin_id"`
	Activity  string    `json:"activity"`
	Timestamp time.Time `json:"timestamp"`
}

var (
	dbAdmin1 *sql.DB
	dbAdmin2 *sql.DB
)

func main() {
	// Konfigurasi koneksi database
	// Koneksi admin1
	db1, err := sql.Open("mysql", "admin1:apahayo@tcp(127.0.0.1:3306)/ph_meter")
	if err != nil {
		log.Fatal(err)
	}
	defer db1.Close()

	// Koneksi admin2
	db2, err := sql.Open("mysql", "admin2:lupa@tcp(127.0.0.1:3306)/ph_meter")
	if err != nil {
		log.Fatal(err)
	}
	defer db2.Close()

	// Cek koneksi ke database admin 1
	err = db1.Ping()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Admin1 berhasil terhubung ke database!")

	// Cek koneksi ke database admin 2
	err = db2.Ping()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Admin2 berhasil terhubung ke database!")

	// Assign koneksi database ke variabel global
	dbAdmin1 = db1
	dbAdmin2 = db2

	// Inisialisasi router Gorilla Mux
	router := mux.NewRouter()

	// Middleware untuk mengaktifkan CORS
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
			if r.Method == "OPTIONS" {
				return
			}
			next.ServeHTTP(w, r)
		})
	})

	// Route untuk endpoint root
	router.HandleFunc("/", handleRoot)

	// Route untuk endpoint login
	router.HandleFunc("/api/login", loginHandler).Methods("POST")
	// Route untuk endpoint logout
	router.HandleFunc("/api/logout", logoutHandler).Methods("POST")

	// Route untuk endpoint menyimpan data pH
	router.HandleFunc("/api/phdata", savephdataHandler).Methods("POST")
	// Route untuk endpoint mengambil data pH berdasarkan ID
	router.HandleFunc("/api/phdata/{id}", getphdataHandler).Methods("GET")
	// Route untuk endpoint memperbarui data pH berdasarkan ID
	router.HandleFunc("/api/phdata/{id}", updatephdataHandler).Methods("PUT")
	// Route untuk endpoint menghapus data pH berdasarkan ID
	router.HandleFunc("/api/phdata/{id}", deletephdataHandler).Methods("DELETE")

	// Jalankan server HTTP dengan Gorilla Mux
	fmt.Println("Server berjalan di port 8081")
	if err := http.ListenAndServe("127.0.0.1:8081", router); err != nil {
		log.Fatal(err)
	}
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		// Mengabaikan permintaan POST dengan respons kosong
		return
	}
	// fmt.Fprintf(w, "Hello, world!")
}

// saveactivitylogToDB saves an activity log to the specified database
func saveactivitylogToDB(db *sql.DB, AdminID string, activity string) error {
	// Prepare statement
	stmt, err := db.Prepare("INSERT INTO activity_log(admin_id, activity) VALUES (?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute statement
	_, err = stmt.Exec(AdminID, activity)
	if err != nil {
		return err
	}

	return nil
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
    // Get admin from the request body
    var credentials struct {
        Admin string `json:"admin"`
    }
    err := json.NewDecoder(r.Body).Decode(&credentials)
    if err != nil {
        http.Error(w, "Failed to get data from body", http.StatusBadRequest)
        return
    }

    // Perform logout by invalidating the session or token
    switch credentials.Admin {
    case "admin1":
        err := saveactivitylogToDB(dbAdmin1, credentials.Admin, "Admin1 logout")
        if err != nil {
            http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
            return
        }
    case "admin2":
        err := saveactivitylogToDB(dbAdmin2, credentials.Admin, "Admin2 logout")
        if err != nil {
            http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
            return
        }
    default:
        w.WriteHeader(http.StatusBadRequest)
        return
    }
    w.WriteHeader(http.StatusOK)
    w.Write([]byte("Logout successful!"))
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	// Get admin and password from the request body
	var credentials struct {
		Admin    string `json:"admin"`
		Password string `json:"password"`
	}
	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		http.Error(w, "Failed to get data from body", http.StatusBadRequest)
		return
	}

	// Perform admin and password validation
	if credentials.Admin == "admin1" && credentials.Password == "apahayo" {
		// Successful login for admin1
		err := saveactivitylogToDB(dbAdmin1, credentials.Admin, "Admin1 login")
		if err != nil {
			http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login successful for admin1!"))
	} else if credentials.Admin == "admin2" && credentials.Password == "lupa" {
		// Successful login for admin2
		err := saveactivitylogToDB(dbAdmin2, credentials.Admin, "Admin2 login")
		if err != nil {
			http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Login successful for admin2!"))
	} else {
		// Invalid admin or password
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Invalid admin or password"))
	}
}

func savephdataHandler(w http.ResponseWriter, r *http.Request) {
	// Periksa jika request adalah POST request
	if r.Method != http.MethodPost {
		http.Error(w, "Metode request tidak valid", http.StatusMethodNotAllowed)
		return
	}

	// Parse request body
	var data struct {
		PhValue float64 `json:"phValue"`
		AdminID string  `json:"adminID"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Kirim nilai pH ke server dan simpan ke database
	log.Printf("Data pH yang diterima: %.2f\n", data.PhValue)
	log.Printf("AdminID: %s\n", data.AdminID)

	// Get the current timestamp
	timestamp := time.Now()

	// Save pH data to database based on admin input
	switch data.AdminID {
	case "admin1":
		// Save pH data to database (admin1)
		id, err := generateNewID()
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := savephdataToDB(dbAdmin1, id, data.AdminID, data.PhValue, timestamp); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Save activity log (admin1)
		if err := saveactivitylogToDB(dbAdmin1, data.AdminID, fmt.Sprintf("pH data saved with ID %d", id)); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	case "admin2":
		// Save pH data to database (admin2)
		id, err := generateNewID()
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := savephdataToDB(dbAdmin2, id, data.AdminID, data.PhValue, timestamp); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Save activity log (admin2)
		if err := saveactivitylogToDB(dbAdmin2, data.AdminID, fmt.Sprintf("pH data saved with ID %d", id)); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	default:
		// AdminID tidak valid
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

// savephdataToDB saves pH data to the specified database
func savephdataToDB(db *sql.DB, id int, AdminID string, value float64, timestamp time.Time) error {
	// Prepare statement
	stmt, err := db.Prepare("INSERT INTO ph_data(id, admin_id, value, timestamp) VALUES (?, ?, ?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute statement
	_, err = stmt.Exec(id, AdminID, value, timestamp)
	if err != nil {
		return err
	}

	return nil
}

// getphdataHandler handles the request to get pH data by ID
func getphdataHandler(w http.ResponseWriter, r *http.Request) {
	// Get ID from request path
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get pH data from database (admin1)
	phdata, err := getphdataFromDB(dbAdmin1, id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Return pH data as JSON response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(struct {
		Value float64 `json:"value"`
	}{
		Value: phdata,
	})
}

// getphdataFromDB retrieves pH data from the specified database by ID
func getphdataFromDB(db *sql.DB, id int) (float64, error) {
	// Prepare statement
	stmt, err := db.Prepare("SELECT value FROM ph_data WHERE id = ?")
	if err != nil {
		return 0.0, err
	}
	defer stmt.Close()

	// Execute statement
	var value float64
	err = stmt.QueryRow(id).Scan(&value)
	if err != nil {
		return 0.01, err
	}
	return value, nil
}

func updatephdataHandler(w http.ResponseWriter, r *http.Request) {
	// Get ID from request path
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	// Check if request is PUT request
	if r.Method != http.MethodPut {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	// Parse request body
	var data struct {
		Value float64 `json:"value"`
	}
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	// Update pH data in database (admin1)
	if err := updatephdataInDB(dbAdmin1, id, data.Value); err != nil {
		http.Error(w, "Failed to update pH data", http.StatusInternalServerError)
		return
	}

	// Save activity log (admin1)
	if err := saveactivitylogToDB(dbAdmin1, "admin1", fmt.Sprintf("Perbarui pH data dengan ID %d", id)); err != nil {
		http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
		return
	}

	// Update pH data in database (admin2)
	if err := updatephdataInDB(dbAdmin2, id, data.Value); err != nil {
		http.Error(w, "Failed to update pH data", http.StatusInternalServerError)
		return
	}

	// Save activity log (admin2)
	if err := saveactivitylogToDB(dbAdmin2, "admin2", fmt.Sprintf("Perbarui pH data dengan ID %d ", id)); err != nil {
		http.Error(w, "Failed to save activity log", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func updatephdataInDB(db *sql.DB, id int, value float64) error {
	stmt, err := db.Prepare("UPDATE ph_data SET value = ? WHERE id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(value, id)
	if err != nil {
		return err
	}
	return nil
}

// deletephdataHandler handles the request to delete pH data by ID
func deletephdataHandler(w http.ResponseWriter, r *http.Request) {
	// Get ID from request path
	vars := mux.Vars(r)
	idStr := vars["id"]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Delete pH data from database (admin1)
	if err := deletephdataFromDB(dbAdmin1, id); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Save activity log (admin1)
	if err := saveactivitylogToDB(dbAdmin1, "admin1", fmt.Sprintf("Hapus pH data dengan ID %d ", id)); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Delete pH data from database (admin2)
	if err := deletephdataFromDB(dbAdmin2, id); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Save activity log (admin2)
	if err := saveactivitylogToDB(dbAdmin2, "admin2", fmt.Sprintf("Hapus pH data dengan ID %d ", id)); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Return success response
	w.WriteHeader(http.StatusOK)
}

// deletephdataFromDB deletes pH data from the specified database
func deletephdataFromDB(db *sql.DB, id int) error {
	// Prepare statement
	stmt, err := db.Prepare("DELETE FROM ph_data WHERE id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute statement
	_, err = stmt.Exec(id)
	if err != nil {
		return err
	}

	// Log the successful deletion
	log.Printf("Deleted pH data with ID %d", id)
	return nil
}

// generateNewID generates a new ID for pH data
func generateNewID() (int, error) {
	for {
		// Generate a random ID between 1 and 1000
		id := rand.Intn(1000) + 1

		// Check if the ID already exists in the database
		if !idExistsInDB(id) {
			return id, nil
		}
	}
}

// idExistsInDB checks if the given ID already exists in the database
func idExistsInDB(id int) bool {
	// Implement your logic to check if the ID exists in the database
	// This is just a placeholder function
	// You need to query the database to check if the ID exists in the relevant table
	// Return true if the ID exists, false otherwise
	return false
}
