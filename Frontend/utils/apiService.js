// utils/apiService.js

import axios from 'axios';

const postData = async (adminID, phValue) => {
  try {
    const response = await axios.post('/api/phdata', {
        phValue: phValue,
        adminID: adminID,
    });
    console.log(response.data);
    // Handle successful response
  } catch (error) {
    console.error(error);
    // Handle error
  }
};

export default postData;
