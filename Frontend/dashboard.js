import React, { useEffect, useState } from 'react';
import axios from 'axios';

const App = () => {
  const [adminName, setAdminName] = useState('');
  const [phData, setPhData] = useState([]);
  const [newPhValue, setNewPhValue] = useState('');

  useEffect(() => {
    setAdminName('Admin'); // Ganti 'Admin' dengan nilai yang sesuai
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('/api/phdata');
      setPhData(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleLogout = () => {
    const data = {
      admin: "admin1",
    };

    axios
      .post('/api/logout', data)
      .then(() => {
        window.location.href = '/login';
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handlePhValueChange = (event) => {
    setNewPhValue(event.target.value);
  };

  const handleSavePhValue = () => {
    const adminID = localStorage.getItem('adminID');
    const data = { phValue: parseFloat(newPhValue), adminID };

    axios
      .post('/api/phdata', data)
      .then(() => {
        setNewPhValue('');
        fetchData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h1>Hello, {adminName}!</h1>
      <button onClick={handleLogout}>Logout</button>
      <h2>Data pH:</h2>
      <ul>
        {phData.map((data) => (
          <li key={data.id}>
            ID: {data.id} - Value: {data.value}
          </li>
        ))}
      </ul>
      <h2>Input pH Value:</h2>
      <input type="number" value={newPhValue} onChange={handlePhValueChange} />
      <button onClick={handleSavePhValue}>Save</button>
    </div>
  );
};

export default App;
