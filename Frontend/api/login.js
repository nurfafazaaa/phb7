const loginHandler = (req, res) => {
    if (req.method === 'POST') {
      // Mendapatkan admin dan password dari body request
      const { admin, password } = req.body;
  
      // Periksa admin dan password
      if (admin === 'admin1' && password === 'apahayo') {
        res.status(200).json({ message: 'Login successful for admin1!' });
      } else if (admin === 'admin2' && password === 'lupa') {
        res.status(200).json({ message: 'Login successful for admin2!' });
      } else {
        res.status(401).json({ message: 'Invalid admin or password' });
      }
    } else {
      res.status(405).end(); // Method Not Allowed
    }
  };
  
  export default loginHandler;
  