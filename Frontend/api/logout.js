// api/logout

const handleLogout = async (req, res) => {
  const { admin } = req.body;

  try {
    // Tambahan program: Hapus status login dari database atau update status menjadi "logged out"
    await database.collection('admins').updateOne(
      { adminID: admin },
      { $set: { isLoggedIn: false } }
    );

    // Contoh: Setelah proses logout berhasil, kirim respon sukses
    res.status(200).json({ message: 'Logout successful' });
  } catch (error) {
    // Jika terjadi kesalahan saat menghapus atau memperbarui status login, kirim respon dengan pesan kesalahan
    res.status(500).json({ error: 'An error occurred during logout' });
  }
};

export default handleLogout;
