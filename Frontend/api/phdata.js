// api/phdata.js

import postData from '../utils/apiService';

export default async function handler(req, res) {
  try {
    const { adminID, phValue } = req.body;

    // Perform any necessary logic, such as saving data to the database

    // Call the postData function from the imported module
    await postData(adminID, phValue);

    // Send a response back to the client
    res.status(200).json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred' });
  }
}
