import { useState } from 'react';
import axios from 'axios';

const Login = () => {
  const [admin, setAdmin] = useState('');
  const [password, setPassword] = useState('');

  const handleAdminChange = (e) => {
    setAdmin(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('/api/login', { admin, password });
      console.log(response.data); // Response dari server

      // Mengalihkan ke halaman dashboard setelah login berhasil
          window.location.href = '/dashboard';
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="admin">Admin:</label>
          <input
            type="text"
            id="admin"
            value={admin}
            onChange={handleAdminChange}
          />
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <button type="submit">Login</button>
      </form>
    </div>
  );
};

export default Login;
